<?php

require \dirname(__DIR__).'/vendor/autoload.php';

use App\Service\IMAPService;
use App\Service\LogService;
use App\Service\StatementService;
use Symfony\Component\Dotenv\Dotenv;

// Environment
$dotenv = new Dotenv();
$dotenv->load(\dirname(__DIR__).'/.env');

// IMAP
$imapService = new IMAPService();
$data = $imapService->getAttachments();

// Log
$logger = new LogService();

// Process attachments
//$data = [
//    'files' => ["/var/www/sigma.kuzmani.ru/public_html/statements-service/statements/21022024042006-1708623756.csv"],
////    'emailFrom' => 'informer@tochka.com',
//    'emailFrom' => 'vtb-inform@vtb.ru',
////    'emailFrom' => 'Messages@alfabank.ru',
//];
if ($data) {
    $logger->log('Start process attachments', null, $data);

    $statementService = new StatementService();

    try {
        $statementService->processStatements($data);
    } catch (Exception $e) {
        $logger->log('Error processStatements: ' . $e->getMessage(), 'error');
    }
}

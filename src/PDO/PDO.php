<?php

declare(strict_types=1);

namespace App\PDO;

class PDO
{
    /**
     * @return \PDO
     */
    public static function createInstance(): \PDO
    {
        try {
            $pdo = new \PDO(
                $_ENV['DB_DRIVER'].':host='.$_ENV['DB_HOST'].';dbname='.$_ENV['DB_DATABASE'].';charset=utf8',
                $_ENV['DB_USERNAME'],
                $_ENV['DB_PASSWORD']
            );
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo 'DB CONNECTION FAILED: '.$e->getMessage() . PHP_EOL;
            exit;
        }

        return $pdo;
    }
}
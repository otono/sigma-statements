<?php

declare(strict_types=1);

namespace App\Model;

class Billing extends AbstractModel
{
    /** @var string  */
    protected $table = 'sigma_Billings';

    /**
     * @param array $statement
     * @return int
     * @throws \Exception
     */
    public function save(array $statement): int
    {
        $billing = $statement;
        unset($billing['account']);
        unset($billing['notes']);

        try {
            $this->pdo->beginTransaction();

            // Save billing
            $sql = "INSERT INTO sigma_Billings (company_id, date, balance_start, balance_end, debet, credit, date_start, date_end, file) VALUES (?,?,?,?,?,?,?,?,?)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array_values($billing));

            $billingId = (int) $this->pdo->lastInsertId();

            // Save notes
            foreach ($statement['notes'] as $note) {
                $note['billing_id'] = $billingId;
                unset($note['balance_start']);
                unset($note['balance_end']);

                $billingNote = new \App\Model\BillingNote($this->pdo);
                $billingNote->save(array_values($note));
            }

            $this->pdo->commit();
        } catch (\Exception $e) {
            $this->pdo->rollBack();
            throw $e;
        }

        return $billingId;
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function exists(string $filename): bool
    {
        $stmt = $this->pdo->prepare("SELECT id FROM sigma_Billings WHERE file = ?");
        $stmt->bindValue(1, $filename);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return \is_array($row);
    }
}
<?php

declare(strict_types=1);

namespace App\Model;

class Order extends AbstractModel
{
    /** @var string  */
    protected $table = 'sigma_Orders';

    /** @var array */
    protected $order;

    // Статусы заказа
    const STATES = [
        'removed'		=> 1,
        'new'			=> 2,
        'priced'		=> 4,
        'send'			=> 8,
        'payed'			=> 16,
        'technolog'		=> 32,
        'production'	=> 64,
        'archive'		=> 128,
        'finished'		=> 256,
        'partner'		=> 512,
        'laser'			=> 1024
    ];

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM ". $this->table ." WHERE OrderID = ?");
        $stmt->execute([$id]);

        $this->order = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($this->order) ? $this : false;
    }

    /**
     * @return float
     */
    public function getOrderSum(): float
    {
        $orderSum = 0;

        // НДС
        $stmt = $this->pdo->prepare("SELECT NDS from sigma_Companies WHERE CompanyID = ?");
        $stmt->execute([$this->order['Company']]);
        $nds = (bool) $stmt->fetchColumn() ? '_nds' : '';

        // Сумма работ
        $stmt = $this->pdo->prepare("SELECT SUM(Count * Price) AS sum, SUM(Count * price_nds) AS sum_nds FROM sigma_Details2Order WHERE OrderID = ?");
        $stmt->execute([$this->order['OrderID']]);
        $sumDetails = $stmt->fetch(\PDO::FETCH_ASSOC);

        $orderSum += ($sumDetails) ? $sumDetails['sum'.$nds] : 0;

        // Сумма материала
        $stmt = $this->pdo->prepare("
            SELECT SUM(sigma_Material2Detail.Price * sigma_Details2Order.Count) as sum,
                   SUM(sigma_Material2Detail.price_nds * sigma_Details2Order.Count) as sum_nds
            FROM sigma_Details2Order, sigma_Material2Detail 
            	WHERE sigma_Details2Order.OrderID = ?
	            AND sigma_Details2Order.DetailID = sigma_Material2Detail.DetailID
            GROUP BY sigma_Details2Order.OrderID
        ");
        $stmt->execute([$this->order['OrderID']]);
        $sumMaterial = $stmt->fetch(\PDO::FETCH_ASSOC);

        // Итог
        $orderSum += ($sumMaterial) ? $sumMaterial['sum'.$nds] : (float) $this->order['DeliverySum'];

        return $orderSum;
    }

    /**
     * @return float
     */
    public function getOrderDebtSum(): float
    {
        $orderSum = $this->getOrderSum();

        $stmt = $this->pdo->prepare("SELECT SUM(Sum) as sum FROM sigma_Payments WHERE OrderID = ?");
        $stmt->execute([$this->order['OrderID']]);

        $payedSum = (float) $stmt->fetchColumn();

        return $orderSum - $payedSum;
    }
}
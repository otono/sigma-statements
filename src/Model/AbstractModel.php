<?php

declare(strict_types=1);

namespace App\Model;

abstract class AbstractModel
{
    /** @var \PDO */
    protected $pdo;

    /**
     * Billing constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo = null)
    {
        if (\is_null($pdo)) {
            $pdo = \App\PDO\PDO::createInstance();
        }
        $this->pdo = $pdo;
    }

//    public function __destruct()
//    {
//        $this->pdo = null;
//    }
}
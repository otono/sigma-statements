<?php

declare(strict_types=1);

namespace App\Model;

class BillingNote extends AbstractModel
{
    /** @var string  */
    protected $table = 'sigma_BillingNotes';

    /**
     * @param array $note
     */
    public function save(array $note)
    {
        $sql = "INSERT INTO sigma_BillingNotes (billing_id, date, code, bik, ks_bank, bank_name, ks, korrespond, doc_num, doc_date, debet, credit, comment, inn) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($note);
    }

    /**
     * @param int $billingId
     * @return array
     */
    public function findByBillingId(int $billingId)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM sigma_BillingNotes WHERE billing_id = ?");
        $stmt->execute([$billingId]);

        $notes = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $notes;
    }

    /**
     * @param int $id
     * @param int $paymentId
     */
    public function setPaymentId(int $id, int $paymentId)
    {
        $stmt = $this->pdo->prepare("UPDATE sigma_BillingNotes SET payment_id = ? WHERE id = ?");
        $stmt->execute([$paymentId, $id]);
    }
}
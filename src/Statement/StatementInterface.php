<?php

declare(strict_types=1);

namespace App\Statement;

interface StatementInterface
{
    /**
     * @param string $path
     * @return array
     */
    public function parse(string $path);

    /**
     * @param string $row
     * @return array
     */
    public function parseRow(string $row): array;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @param string $contents
     */
    public function setAccountNumber(string $contents);

    /**
     * @param string $name
     * @return mixed
     */
    public function setFile(string $name);

    /**
     * @param array $accountToIds
     * @return mixed
     */
    public function setCompanyId(array $accountToIds);

    /**
     * @return bool
     */
    public function isEmpty(): bool;
}
<?php

declare(strict_types=1);

namespace App\Statement;

class AlfaBankStatement extends Statement
{
    // Первая строка с данными
    const ROW_START = 2;

    // Номера колонок необходимых полей
    const FIELDS = [
        'date' => 29,
        'code' => 80,
        'bik' => 39,
        'ks_bank' => 40,
        'bank_name' => 38,
        'ks' => 48,
        'korrespond' => 34,
        'doc_num' => 30,
        'doc_date' => 31,
        'debet' => 33,
        'credit' => 33,
        'comment' => 65,
        'inn' => 35,
        'date_debet' => 29,
        'date_credit' => 29,
        // Additional
        'balance_start' => 16,
        'balance_end' => 24,
        'payer_rs' => 37,
        'recipient_rs' => 45,
        'statement_rs' => 8,
    ];

    /**
     * @param string $contents
     * @return false|void
     */
    public function setAccountNumber(string $contents)
    {
        $rows = explode(PHP_EOL, $contents);

        if (!isset($rows[static::ROW_START])) {
            return false;
        }

        $data = explode(self::detectDelimiter($rows[static::ROW_START]), $rows[static::ROW_START]);

        $this->statement['account'] = ($data[static::FIELDS['statement_rs']] == $data[static::FIELDS['recipient_rs']])
            ? StatementNote::parseField($data[static::FIELDS['recipient_rs']])
            : StatementNote::parseField($data[static::FIELDS['payer_rs']]);
    }
}
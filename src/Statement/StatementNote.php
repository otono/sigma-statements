<?php

declare(strict_types=1);

namespace App\Statement;

class StatementNote
{
    public array $note;

    public string $account;

    /**
     * StatementNote constructor.
     * @param string $row
     * @param array $fields
     * @param string $account
     */
    public function __construct(string $row, array $fields, string $account)
    {
        $this->account = $account;
        $this->note = $this->parse($row, $fields);
    }

    /**
     * @param string $row
     * @param array $fields
     * @return array
     */
    public function parse(string $row, array $fields): array
    {
        $row = str_getcsv($row, Statement::detectDelimiter($row));

        $debet = ($this->parseField($row[$fields['payer_rs']]) == $this->account) ? $row[$fields['debet']] : '0';
        $credit = ($this->parseField($row[$fields['recipient_rs']]) == $this->account) ? $row[$fields['credit']] : '0';

        return [
            'billing_id' => null,
            'date' => $row[$fields['date']],
            'code' => (int) $row[$fields['code']],
            'bik' => $this->parseField($row[$fields['bik']]),
            'ks_bank' => $this->parseField($row[$fields['ks_bank']]),
            'bank_name' => $row[$fields['bank_name']],
            'ks' => $this->parseField($row[$fields['ks']]),
            'korrespond' => str_replace('"', '', $row[$fields['korrespond']]),
            'doc_num' => $row[$fields['doc_num']],
            'doc_date' => $row[$fields['doc_date']],
            'debet' => $debet,
            'credit' => $credit,
            'comment' => $row[$fields['comment']],
            'inn' => $this->parseField($row[$fields['inn']]),
            'balance_start' => $row[$fields['balance_start']],
            'balance_end' => $row[$fields['balance_end']],
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->note;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function parseField(string $value): string
    {
        return (substr($value, 0, 1) == "'") ? substr($value, 1) : $value;
    }
}
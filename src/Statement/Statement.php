<?php

declare(strict_types=1);

namespace App\Statement;

abstract class Statement implements StatementInterface
{
    public const CONVERT_ENCODING = true;

    public const EMAIL_FROM_ALFA = 'Messages@alfabank.ru';
    public const EMAIL_FROM_TOCHKA = 'informer@tochka.com';
    public const EMAIL_FROM_VTB = 'vtb-inform@vtb.ru';

    public const MAP_EMAIL_STATEMENT = [
        self::EMAIL_FROM_ALFA => AlfaBankStatement::class,
        self::EMAIL_FROM_TOCHKA => TochkaBankStatement::class,
        self::EMAIL_FROM_VTB => VtbBankStatement::class,
    ];

    protected array $statement = [
        'company_id' => null,
        'date' => null,
        'balance_start' => 0,
        'balance_end' => 0,
        'debet' => 0,
        'credit' => 0,
        'date_start' => null,
        'date_end' => null,
        'file' => null,
        'account' => null,
        'notes' => [],
    ];

    /**
     * Statement constructor.
     * @param string $contents
     */
    public function __construct(string $contents)
    {
        $this->setAccountNumber($contents);
        $this->parse($contents);

        if (!$this->isEmpty()) {
            $this->getInfo();
        }
    }

    /**
     * Detect delimiter
     * @param string $row
     * @return string
     */
    public static function detectDelimiter(string $row): string
    {
        $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($row, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }

    /**
     * @param string $contents
     */
    public function parse(string $contents)
    {
        $rows = explode(PHP_EOL, $contents);

        // Parse statements notes
        foreach ($rows as $num => $row) {
            if ($num < static::ROW_START || $row == '') {
                continue;
            }
            $this->statement['notes'][] = $this->parseRow($row);
        }
    }

    /**
     * @param string $row
     * @return array
     */
    public function parseRow(string $row): array
    {
        return (new StatementNote($row, static::FIELDS, $this->statement['account']))->toArray();
    }

    protected function getInfo()
    {
        $this->statement['date'] = date('Y-m-d H:i:s');

        // Входящий/исходящий остаток
        $this->statement['balance_start'] = $this->statement['notes'][0]['balance_start'];
        $this->statement['balance_end'] = end($this->statement['notes'])['balance_end'];

        // Дебет/кредит
        foreach ($this->statement['notes'] as $note) {
            $this->statement['debet'] += (float) $note['debet'];
            $this->statement['credit'] += (float) $note['credit'];
            if (\is_null($this->statement['date_start']) || strtotime($note['date']) < strtotime($this->statement['date_start'])) {
                $this->statement['date_start'] = date('Y-m-d', strtotime($note['date']));
            }
            if (\is_null($this->statement['date_end']) || strtotime($note['date']) < strtotime($this->statement['date_end'])) {
                $this->statement['date_end'] = date('Y-m-d', strtotime($note['date']));
            }
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->statement;
    }

    /**
     * Set statement account number
     * @param string $contents
     * @return false
     */
    public function setAccountNumber(string $contents)
    {
        $rows = explode(PHP_EOL, $contents);

        if (!isset($rows[static::ROW_START])) {
            return false;
        }

        $data = explode(self::detectDelimiter($rows[static::ROW_START]), $rows[static::ROW_START]);
        $this->statement['account'] = ($data[static::FIELDS['date_debet']] !== '')
            ? StatementNote::parseField($data[static::FIELDS['payer_rs']])
            : StatementNote::parseField($data[static::FIELDS['recipient_rs']]);
    }

    /**
     * @param string $name
     */
    public function setFile(string $name)
    {
        $this->statement['file'] = $name;
    }

    /**
     * @param array $companies
     * @return mixed|void
     */
    public function setCompanyId(array $companies)
    {
        foreach ($companies as $company) {
            if ($company['RS'] == $this->statement['account']) {
                $this->statement['company_id'] = (int) $company['CompanyID'];
            }
        }
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return count($this->statement['notes']) == 0;
    }
}
<?php

declare(strict_types=1);

namespace App\Statement;

class TochkaBankStatement extends Statement
{
    public const CONVERT_ENCODING = false;

    // Первая строка с данными
    const ROW_START = 1;

    // Номера колонок необходимых полей
    const FIELDS = [
        'date' => 0,
        'code' => 41,
        'bik' => 15,
        'ks_bank' => 18,
        'bank_name' => 16,
        'ks' => 26,
        'korrespond' => 11,
        'doc_num' => 4,
        'doc_date' => 1,
        'debet' => 7,
        'credit' => 7,
        'comment' => 10,
        'inn' => 13,
        'date_debet' => 2,
        'date_credit' => 3,
        // Additional
        'balance_start' => 8,
        'balance_end' => 9,
        'payer_rs' => 12,
        'recipient_rs' => 20,
    ];
}
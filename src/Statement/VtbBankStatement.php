<?php

namespace App\Statement;

use App\Statement\Statement;

class VtbBankStatement extends Statement
{
    // Первая строка с данными
    const ROW_START = 1;
    // Сколько строк снизу не обрабатывать
    const COUNT_END_ROWS_NOT_NOTES = 3;

    // Номера колонок необходимых полей
    const FIELDS = [
        'date' => 4,
        'code' => 6,
        'bik' => 21,
        'ks_bank' => 20,
        'bank_name' => 22,
        'ks' => 15,
        'korrespond' => 18,
        'doc_num' => 5,
        'doc_date' => 4,
        'debet' => 7,
        'credit' => 7,
        'comment' => 9,
        'inn' => 19,
        'date_debet' => 3,
        'date_credit' => 2,
        // Additional
        'balance_start' => 37,  // Эти значения у ВТБ не на каждой строке,
        'balance_end' => 38,    // а внизу отдельно
        'payer_rs' => 17,
        'recipient_rs' => 11,
    ];

    /** @var int  */
    public const BALANCE_ROW_ACCOUNT_COL = 0;
    /** @var int Входящий остаток */
    public const BALANCE_START_COL = 3;
    /** @var int Исходящий остаток */
    public const BALANCE_END_COL = 4;

    /**
     * @param string $contents
     * @return void
     */
    public function parse(string $contents)
    {
        $rows = array_filter(explode(PHP_EOL, $contents));

        // Get balance_start/balance_end
        $delimiter = static::detectDelimiter(end($rows));
        $balanceRow = str_getcsv(end($rows), $delimiter);

        // Parse statements notes
        foreach ($rows as $num => $row) {
            if (
                $num < static::ROW_START || $row == ''
                || $num > count($rows) - self::COUNT_END_ROWS_NOT_NOTES
            ) {
                continue;
            }

            $row .= $delimiter . $balanceRow[self::BALANCE_START_COL];
            $row .= $delimiter . $balanceRow[self::BALANCE_END_COL];

            $this->statement['notes'][] = $this->parseRow($row);
        }
    }

    /**
     * @param string $contents
     * @return false|void
     */
    public function setAccountNumber(string $contents)
    {
        $rows = array_filter(explode(PHP_EOL, $contents));

        if (!isset($rows[static::ROW_START])) {
            return false;
        }

        $delimiter = static::detectDelimiter(end($rows));
        $balanceRow = str_getcsv(end($rows), $delimiter);

        $this->statement['account'] = $balanceRow[self::BALANCE_ROW_ACCOUNT_COL];
    }
}

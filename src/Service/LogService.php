<?php

namespace App\Service;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogService
{
    /** @var string Log file */
    private const LOG_FILE = '/log/log.log';

    /** @var Logger */
    private Logger $log;

    public function __construct()
    {
        $this->log = new Logger('log');
        $this->log->pushHandler(new StreamHandler(\dirname(__DIR__, 2) . self::LOG_FILE));
    }

    /**
     * @param string $text
     * @param string|null $level
     * @param array $context
     * @return void
     */
    public function log(string $text, ?string $level = null, array $context = []): void
    {
        if (
            $level
            && method_exists($this->log, $level)
        ) {
            $this->log->{$level}($text, $context);
        } else {
            $this->log->info($text, $context);
        }
    }
}
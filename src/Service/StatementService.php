<?php

namespace App\Service;

use App\Statement\Statement;

class StatementService
{
    /**
     * @param array $data
     * @throws \Exception
     */
    public function processStatements(array $data)
    {
        // PDO
        $pdo = \App\PDO\PDO::createInstance();

        // Parse files
        foreach ($data['files'] as $file) {
            $contents = file_get_contents($file);
            $statementClass = Statement::MAP_EMAIL_STATEMENT[$data['emailFrom']];

            if (constant($statementClass . '::CONVERT_ENCODING')) {
                $contents = mb_convert_encoding(file_get_contents($file), 'UTF-8', 'WINDOWS-1251');
            }

            $statement = new $statementClass($contents);

            // Пустая выписка
            if ($statement->isEmpty()) {
                continue;
            }

            // Set filename
            $statement->setFile(basename($file));

            // Set Company ID
            $companiesQuery = $pdo->query("SELECT CompanyID, RS FROM sigma_Companies");
            $companies = $companiesQuery->fetchAll(\PDO::FETCH_ASSOC);
            $statement->setCompanyId($companies);

            // Save
            $stmt = $statement->toArray();
            $billing = new \App\Model\Billing($pdo);
            $billingId = $billing->save($stmt);

            // Идентификация платежей
            (new \App\Service\PaymentIdentificationService())->identPayments($billingId, $stmt['company_id']);
        }

        // Close DB connection
        $pdo = null;
    }
}

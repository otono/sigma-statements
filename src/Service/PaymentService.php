<?php

declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client;

class PaymentService
{
    /**
     * @param array $data
     */
    public function addPayment(array $data)
    {
        $client = new Client();

//        try {
            $response = $client->get($_ENV['ROBOT_PAYMENT_ENDPOINT'], [
                'headers' => [
                    'X-Robot-Service' => $this->getToken($data),
                ],
                'query' => $data,
            ]);
//        } catch (\Exception $e) {
//            // @TODO Logger ...
//        }

        $res = json_decode($response->getBody()->getContents(), true);

        return (isset($res['payment_id'])) ? $res['payment_id'] : false;
    }

    /**
     * @param array $data
     * @return string
     */
    private function getToken(array $data): string
    {
        return sha1(implode($data) . $_ENV['ROBOT_TOKEN_SALT']);
    }
}
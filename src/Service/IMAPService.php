<?php

namespace App\Service;

use App\Statement\Statement;

class IMAPService
{
    public CONST VALID_ADDRESS = [
        Statement::EMAIL_FROM_ALFA,
        Statement::EMAIL_FROM_TOCHKA,
        Statement::EMAIL_FROM_VTB,
    ];

    /** @var mixed */
    protected $mailbox;

    /**
     * Get statement attachments
     */
    public function getAttachments()
    {
        $pdo = \App\PDO\PDO::createInstance();

        $this->mailbox = imap_open($_ENV['MAIL_INBOX'], $_ENV['MAIL_USERNAME'], $_ENV['MAIL_PASSWORD'])
            or die('Cannot connect to Gmail: ' . imap_last_error());

        $statementMails = imap_search($this->mailbox, 'SUBJECT "Выписка" UNSEEN');

        $attachments = [];
        $files = [];
        $filePath = \dirname(__DIR__, 2).'/statements/';

        // No new mails
        if (!$statementMails) {
            // Close connection
            imap_close($this->mailbox);
            return false;
        }

        $emailFrom = null;

        foreach ($statementMails as $mail) {
            $headers = imap_headerinfo($this->mailbox, $mail);
            preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/", $headers->fromaddress, $emailFrom);
            $emailFrom = reset($emailFrom);

            if (\in_array($emailFrom, self::VALID_ADDRESS)) {
                // Set filenmae
                $filename = date('dmYHis', strtotime($headers->MailDate)).'-'.time();

                // Check billing exists
                $billing = new \App\Model\Billing($pdo);
                if ($billing->exists($filename)) {
                    continue;
                }

                $structure = imap_fetchstructure($this->mailbox, $mail);

                // Get attachments
                // @TODO переделать через генераторы
                foreach ($structure->parts as $key => $part) {
                    if (isset($part->disposition) && $part->disposition === 'ATTACHMENT') {
                        $attachments[$key] = $this->processAttachmentPart($part, $key+1, $mail);
                        $attachments[$key]['filename'] = $filename .'.csv';
                    } elseif (isset($part->parts)) {
                        foreach ($part->parts as $key2 => $part2) {
                            if (isset($part2->disposition) && $part2->disposition === 'ATTACHMENT') {
                                $attachments[$key2] = $this->processAttachmentPart($part2, ($key+1).'.'.($key2+1), $mail);
                                $attachments[$key2]['filename'] = $filename;
                            }
                        }
                    }
                }

                // Save attachment
                if (count($attachments) > 0){
                    foreach ($attachments as $attachment){
                        file_put_contents($filePath.$attachment['filename'], $attachment['attachment']);
                        $files[] = $filePath.$attachment['filename'];
                    }
                }
            }
        }

        // Close DB connection
        $pdo = null;

        // Close connection
        imap_close($this->mailbox);

        return [
            'files' => $files,
            'emailFrom' => $emailFrom,
        ];
    }

    /**
     * @param $part
     * @param $section
     * @param $mail
     * @return array
     */
    protected function processAttachmentPart($part, $section, $mail): array
    {
        foreach ($part->parameters  as $param) {
            if ($param->attribute === 'NAME') {
                $attachment['name'] = imap_utf8($param->value);
            }
        }

        $attachment['attachment'] = imap_fetchbody($this->mailbox, $mail, $section);

        // 3 = BASE64
        if ($part->encoding == 3) {
            $attachment['attachment'] = base64_decode($attachment['attachment']);
        }
        // 4 = QUOTED-PRINTABLE
        elseif ($part->encoding == 4) {
            $attachment['attachment'] = quoted_printable_decode($attachment['attachment']);
        }

        return $attachment;
    }
}
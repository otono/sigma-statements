<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\BillingNote;
use App\Model\Order;

/**
 * Идентификация платежей на заказ
 * @package App\Service
 */
class PaymentIdentificationService
{
    /**
     * @param $billingId
     * @param $companyId
     */
    public function identPayments($billingId, $companyId)
    {
        $billingNote = new BillingNote();
        $notes = $billingNote->findByBillingId($billingId);
        $paymentService = new PaymentService();

        foreach ($notes as $key => $note) {
            // Смотрим только входящие платежи
            if ((int) $note['credit'] == 0) {
                continue;
            }

            // Ищем 5-значный номер заказа в наименовании платежа
            if (preg_match_all("/\b\d{5}\b/", $note['comment'], $matches)) {
                $ordersIds = $this->clearMatches($note['comment'], $matches);

                // Автоматом оплачиваем только однозначно идентифицированные платежи
                if (count($ordersIds) !== 1) {
                    continue;
                }

                $orderId = (int) $ordersIds[0];

                // Проверяем заказ
                $order = (new Order())->findById($orderId);

                if (!$order) {
                    continue;
                }

                // Если сумма платежа больше остатка по заказу, пропускаем
//                if ((float) $note['credit'] > $order->getOrderDebtSum() + 1 ) {
//                    continue;
//                }

                // Отправляем запрос на добавление платежа
                $paymentId = $paymentService->addPayment([
                    'OrderID' => $orderId,
                    'Sum' => $note['credit'],
                    'sums' => '',
                    'Date' => $note['date'],
                    'CompanyName' => $note['korrespond'],
                    'PayComment' => $note['comment'],
                    'BillNumber' => $note['doc_num'],
                    'company_id' => $companyId,
                ]);

                // Связь с платежом
                if ($paymentId) {
                    $billingNote->setPaymentId((int) $note['id'], (int) $paymentId);
                }
            }
        }
    }

    /**
     * @param string $comment
     * @param array $matches
     * @return array
     */
    private function clearMatches(string $comment, array $matches): array
    {
        $orderIds = $matches[0];

        // Проверка суммы с разделителем на копейки
        foreach (['.', ',', '-'] as $delimiter) {
            // Проверяем, не сумма ли определилась как номер заказа
            preg_match_all("/\b\d{5}".$delimiter."\d{2}\b/", $comment, $matchesSum);

            foreach ($orderIds as $key => $orderId) {
                foreach ($matchesSum[0] as $match) {
                    if (strstr($match, $delimiter, true) == $orderId) {
                        unset($orderIds[$key]);
                    }
                }
            }
        }
        // Проверка суммы без копеек
        foreach (['ру'] as $delimiter) {
            preg_match_all("/\b\d{5}\s".$delimiter."/", $comment, $matchesSum);
            foreach ($orderIds as $key => $orderId) {
                foreach ($matchesSum[0] as $match) {
                    if (strstr($match, ' '.$delimiter, true) == $orderId) {
                        unset($orderIds[$key]);
                    }
                }
            }
        }

        return array_unique($orderIds);
    }
}